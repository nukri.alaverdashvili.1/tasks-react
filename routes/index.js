const express = require('express')
const router = express.Router();
const User = require('../models/User')
const Status = require('../models/status')
const bcrypt = require('bcryptjs')
const jwt = require('express-jwt');
const jsonwebtoken = require('jsonwebtoken');


const jwtSecret = 'secret123';

//status ების მიღებისთვის
router.get('/status', async (req,res)=>{
  try{
    const status = await Status.find();
    res.json(status)
  }catch(error){
    res.json({server:'Server Error 505'})
  }
})


router.post('/user/:id', async (req,res)=>{
  try{
    const user= await User.findById(req.params.id)
    if(user)
    {
      res.json(user._id)
    }
  }catch(err){

  }
})

//Login
router.post('/login', async (req,res)=>{
  try{
    const user = await User.findOne({email:req.body.email})
    if(user){
      bcrypt.compare(req.body.password, user.password, (err,isMatch)=>{
        if(isMatch)
        {
          res.json({token:jsonwebtoken.sign({id:user._id, email:user.email},jwtSecret)}) //ვაგზავნით ტოკენს რომელიც კოდირებული იქნება იუზერის აიდი და მეილი
        }else{
          res.json({msg:'Password is Incorrect'})
        }
      })
    }else{
      res.json({msg:'Email is Wrong'}) //თუ იუზერის იმეილი არ ემთხვევა მონაცემთა ბაზაში მეილებს გაგზავნის ამ ერორის მესიჯს.
    }
    }catch(err)
    {
      res.json({server:'Server Error 505'})
    }
})


// Registration
router.post('/saveUser', async (req,res)=>{
  if(req.body.googleId)
  {
    try{
      const userExist = await User.findOne({googleId:req.body.googleId})
      if(!userExist){
        const user = await new User({googleId:req.body.googleId, email:req.body.email, name:req.body.name})
        user.save()
        res.json({token:jsonwebtoken.sign({ud:user._id, email:user.email}, jwtSecret)})
      }else{
        res.json({token:jsonwebtoken.sign({id:userExist._id, email:userExist.email}, jwtSecret)})
      }
    }catch(err)
    {
      res.json({msg:err})
    }
  }
  else{
    try{
      const userExist = await User.findOne({email:req.body.email}) //ეძებს იუზერს მეილის მიხედვით
      if(!userExist) //თუ არ არსებობს იუზერი მაშინ შემოდის იფში და ინახავს ახალ იუზერს
      {
        const salt = await bcrypt.genSalt() 
        const hashPassword = await bcrypt.hash(req.body.password, salt) //პაროლს კოდირებას გაუკეთებს
        const user = await new User({email:req.body.email, name:req.body.name, password:hashPassword})
        user.save()
        res.json(user) // გადავცემთ უკან იუზერის ინფორმაციას.
      }else{
        res.json({msg:'User already exists'}) // თუ იუზერი არსებობს მაშინ გადავცემთ შეტყობინებას რომ უკვე არსებობს
      }
    }catch (err){ // ამ შემთხვევაში გადავცემთ ერორის მესიჯს. err.message თუ დავწერთ კონკრეტულად რისი ერორიც არის იმას გაუგზავნის
      res.json({msg:'Server Error 500'})
    }
  }
})

module.exports = router;
