const express = require('express')
const router = express.Router();
const Task = require('../../models/Task')
const cors = require('cors');
const { json } = require('body-parser');

//users
router.post('/saveTasks/:id', async (req,res)=>{
    let date = new Date(req.body.date);
    date.setMinutes( date.getMinutes() + 480 );
    const task = await new Task({task:req.body.task, date:date, status:req.body.status,user:req.params.id})
    await task.save()
    .then((task)=>res.json(task))
    .catch(err => res.status(400).json('Error:' +err));
  })
  
  //Get User Tasks
  router.post('/getTasks', async (req,res)=>{
    try{
      const task = await Task.find({user:req.body.id})
      res.json(task)
    }catch(err){
      res.json({msg:err.message})
    }
  })
  //Delete Task
  router.delete('/delete/:id', async (req,res)=>{
    try{
      const task = await Task.findById(req.params.id)
      task.delete()
      res.json(true)
    }catch(err){
      res.json({msg:err.message})
    }
  })

  // Get Task
  router.get('/getTask/:id', async (req,res)=>{
    try{
      const task = await Task.findById(req.params.id)
      res.json(task)
    }catch(error){
      res.json({msg:error.message})
    }
  })
  
//Update Task
  router.put('/updateTask/:id',async(req,res)=>{
    try{
      const task = await Task.findById(req.params.id)
      let date = new Date(req.body.date);
      date.setMinutes( date.getMinutes() + 480 );
      task.task=req.body.task,
      task.date=date,
      task.status=req.body.status
      task.save()
      res.json(task)
    }catch(err){
      res.json({msg:'Server Error 505'})
    }
  })

  module.exports = router;