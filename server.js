const express = require('express')
const app = express()
const expressLayouts = require('express-ejs-layouts')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const cors = require('cors')

//DB config 
dotenv.config();
const PORT = process.env.PORT||5000;

//Connect to Mongo
mongoose.connect(process.env.DB_CONNECT, {useNewUrlParser:true}) //დაკავშირება მონგოსთა, userNewUrlParser გვჭირდება ახალი ვერსიისთვის, თუ არ გავუწერთ ერორები გვექნება
.then(()=>console.log('Mongodb Connected...')) // გამოაქვს შეტყობინება წარმატებული დაკავშირების შემდეგ
.catch(err=>console.log(err)); //ერორის სემთხვევაში დაბეჭდოს ეს ერორი

//bodyparser
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ limit:'25000mb', extended:false})) //ამის დახმარებით მივიღებთ req.body დან მონაცემებს
app.use(cors())

app.use('/', require('./routes/index'))
app.use('/profile',require('./routes/profile/task'))


app.listen(PORT, console.log(`Server started on port ${PORT}`))
