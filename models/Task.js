const mongoose = require('mongoose')

const TaskSchema = new mongoose.Schema({
    task:{
        type: String,
        required:true
    },
    date:{
        type: Date,
        required:true,
    },
    status:{
        type: mongoose.Schema.Types.ObjectId,
        required:true,
        ref:'Status'
    }, 
    user:{
        type:mongoose.Schema.Types.ObjectID,
        required:true,
        ref:'User'
    }
});

const Task = mongoose.model('Task',TaskSchema)
module.exports = Task;